
async function login(credentials) {
    let response = await fetch(
        `${API_URL}/users/login`,
        {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(credentials)
        }
    );
    return await handleJsonResponse(response);
}

async function fetchCompanies() {
    let response = await fetch(
        `${API_URL}/companies`,
        {
            method: 'GET',
            headers: { 'Authorization': `Bearer ${getToken()}` }
        }
    );
    return await handleJsonResponse(response);
}

async function fetchCompany(id) {
    let response = await fetch(
        `${API_URL}/companies/${id}`,
        {
            method: 'GET',
            headers: { 'Authorization': `Bearer ${getToken()}` }
        }
    );
    return await handleJsonResponse(response);
}

async function postCompany(company) {
    let response = await fetch(
        `${API_URL}/companies`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${getToken()}`
            },
            body: JSON.stringify(company)
        }
    );
    handleResponse(response);
}

async function deleteCompany(id) {
    let response = await fetch(
        `${API_URL}/companies/${id}`,
        {
            method: 'DELETE',
            headers: { 'Authorization': `Bearer ${getToken()}` }
        }
    );
    handleResponse(response);
}

async function postFile(file) {
    let formData = new FormData();
    formData.append("file", file);

    let response = await fetch(
        `${API_URL}/files/upload`,
        {
            method: 'POST',
            headers: { 'Authorization': `Bearer ${getToken()}` },
            body: formData
        }
    );
    return await handleJsonResponse(response);
}

