
// Initialization after the HTML document has been loaded...
window.addEventListener('DOMContentLoaded', () => {
    doLoadItems();
    displaySessionBox();
});

/* 
    --------------------------------------------
    ACTION FUNCTIONS
    --------------------------------------------
*/

async function doLogin() {
    try {
        const credentials = {
            username: document.querySelector('#loginUsername').value,
            password: document.querySelector('#loginPassword').value,
        };
        const session = await login(credentials);
        storeAuthentication(session);
        displaySessionBox();
        closePopup();
        doLoadItems();
    } catch (e) {
        handleError(e, displayLoginPopup);
    }
}

function doLogout() {
    clearAuthentication();
    displayLoginPopup();
}

async function doLoadItems() {
    try {
        const items = await fetchCompanies();
        displayItems(items);
        displayWrapper();
    } catch (e) {
        handleError(e, displayLoginPopup);
    }
}

async function doSaveItem() {
    try {
        // Reading the company data from the company form...
        let item = {
            id: document.querySelector('#itemEditId').value > 0 ? document.querySelector('#itemEditId').value : 0,
            name: document.querySelector('#itemEditName').value,
            logo: document.querySelector('#itemEditLogo').value,
            established: document.querySelector('#itemEditEstablished').value,
            employees: document.querySelector('#itemEditEmployees').value,
            revenue: document.querySelector('#itemEditRevenue').value,
            netIncome: document.querySelector('#itemEditNetIncome').value,
            securities: document.querySelector('#itemEditSecurities').value,
            securityPrice: document.querySelector('#itemEditSecurityPrice').value,
            dividends: document.querySelector('#itemEditDividends').value
        };

        if (validateItem(item)) {
            // Saving the company...
            await postCompany(item);
            closePopup();
            doLoadItems();
        }
    } catch (e) {
        handleError(e, displayLoginPopup);
    }
}

async function doDeleteItem(id) {
    try {
        if (confirm('Soovid sa tõesti seda ettevõtet kustutada?')) {
            await deleteCompany(id);
            doLoadItems();
        }
    } catch (e) {
        handleError(e, displayLoginPopup);
    }
}

async function doUploadFile() {
    try {
        const logoFile = document.querySelector('#file').files[0];
        if (logoFile) {
            let uploadResponse = await postFile(logoFile);
            document.querySelector('#itemEditLogoImage').src = uploadResponse.url;
            document.querySelector('#itemEditLogo').value = uploadResponse.url;
        }
    } catch (e) {
        handleError(e, displayLoginPopup);
    }
}

async function doLoadChart() {
    try {
        const items = await fetchCompanies();
        displayChart(items);
        displayWrapper();
    } catch (e) {
        handleError(e, displayLoginPopup);
    }
}

/* 
    --------------------------------------------
    DISPLAY FUNCTIONS
    --------------------------------------------
*/

function displayLoginPopup() {
    document.querySelector('#wrapper').style.display = 'none';
    openPopup(POPUP_CONF_BLANK_300_300, 'loginFormTemplate');
}

function displaySessionBox() {
    const sessionBox = document.querySelector('header .header-cell-right');
    if (!isEmpty(getToken())) {
        sessionBox.innerHTML = /*html*/`<strong>${getUsername()}</strong> | <a href="javascript:doLogout()">logi välja</a>`;
    }
}

async function displayItemEditPopup(id) {
    await openPopup(POPUP_CONF_DEFAULT, 'itemEditFormTemplate');

    // Clearing the possible previous errors...
    displayErrorBox([]);

    // Clearing the company edit form from previous data...
    document.querySelector('#itemEditId').value = '';
    document.querySelector('#itemEditName').value = '';
    document.querySelector('#itemEditLogoImage').value = '';
    document.querySelector('#itemEditLogo').value = '';
    document.querySelector('#itemEditEstablished').value = '';
    document.querySelector('#itemEditEmployees').value = '';
    document.querySelector('#itemEditRevenue').value = '';
    document.querySelector('#itemEditNetIncome').value = '';
    document.querySelector('#itemEditSecurities').value = '';
    document.querySelector('#itemEditSecurityPrice').value = '';
    document.querySelector('#itemEditDividends').value = '';

    // Loading the company, if id specified...
    if (id > 0) {
        let item = null;
        try {
            item = await fetchCompany(id);
        } catch (e) {
            handleError(e, displayLoginPopup);
        }

        // Filling the company edit form...
        document.querySelector('#itemEditId').value = item.id;
        document.querySelector('#itemEditName').value = item.name;
        document.querySelector('#itemEditLogoImage').src = item.logo;
        document.querySelector('#itemEditLogo').value = item.logo;
        document.querySelector('#itemEditEstablished').value = item.established;
        document.querySelector('#itemEditEmployees').value = item.employees;
        document.querySelector('#itemEditRevenue').value = item.revenue;
        document.querySelector('#itemEditNetIncome').value = item.netIncome;
        document.querySelector('#itemEditSecurities').value = item.securities;
        document.querySelector('#itemEditSecurityPrice').value = item.securityPrice;
        document.querySelector('#itemEditDividends').value = item.dividends;
    }
}

function displayWrapper() {
    document.querySelector('#wrapper').style.display = 'grid';
}

function displayItems(items) {
    let itemsHtml = /*html*/`
        <div class="items">
            <div class="item-fluid">
                <div class="item-100-center">
                    <h1>Börsiettevõtted</h1>
                </div>
            </div>
            <div class="item-fluid">
                <div class="item-100-center">
                    <button id="displayModeButton" onclick="doLoadChart()">Näita diagrammi</button>
                </div>
            </div>
        </div>
        <section class="items">
    `;
    for (let i = 0; i < items.length; i++) {
        itemsHtml = itemsHtml + displayItem(items[i]);
    }
    itemsHtml = itemsHtml + /*html*/`
        </section>
        <div class="items">
            <div class="item-fluid">
                <div class="item-100-center">
                    <button onclick="displayItemEditPopup()">Lisa ettevõte</button>
                </div>
            </div>
        </div>
    `;
    document.querySelector('main').innerHTML = itemsHtml;
}

function displayItem(item) {
    let itemHtml = /*html*/`
            <article class="item">
                <h2 class="item-100">${item.name}</h2>
                <div class="item-100"><img class="item-logo" src="${item.logo}"></div>
                <div class="item-50-bold">Asutatud</div>
                <div class="item-50">${item.established}</div>
                <div class="item-50-bold">Töötajaid</div>
                <div class="item-50">${formatNumber(item.employees)}</div>
                <div class="item-50-bold">Müügitulu</div>
                <div class="item-50">${formatNumber(item.revenue)} &euro;</div>
                <div class="item-50-bold">Netotulu</div>
                <div class="item-50">${formatNumber(item.netIncome)} &euro;</div>
                <div class="item-50-bold">Turuväärtus</div>
                <div class="item-50">${formatNumber(parseInt(item.marketCapitalization))} &euro;</div>
                <div class="item-50-bold">Dividendimäär</div>
                <div class="item-50">${formatNumber(item.dividendYield * 100)} %</div>
                <div class="item-100-center">
                    <button onclick="displayItemEditPopup(${item.id})">Muuda</button> 
                    <button onclick="doDeleteItem(${item.id})" class="button-red">Kustuta</button>
                </div>
            </article>
        `;
    return itemHtml;
}

function displayErrorBox(errors) {
    const itemEditFormContainer = document.querySelector('#itemEditFormContainer');
    const errorBox = document.querySelector('#errorBox');
    if (errors.length > 0) {
        errorBox.style.display = 'block';

        let errorsHtml = /*html*/`<div><strong>Palun paranda järgmised vead:</strong></div>`;
        for (let i = 0; i < errors.length; i++) {
            errorsHtml = errorsHtml + /*html*/`
                <div>
                    ${errors[i]}
                </div>
            `;
        }

        errorBox.innerHTML = errorsHtml;
        itemEditFormContainer.scrollTop = 0;
    } else {
        errorBox.innerHTML = '';
    }
}

function displayChart(items) {
    let chartHtml = /*html*/`
        <div class="items">
            <div class="item-fluid">
                <div class="item-100-center">
                    <h1>Börsiettevõtted</h1>
                </div>
            </div>
            <div class="item-fluid">
                <div class="item-100-center">
                    <button id="displayModeButton" onclick="doLoadItems()">Näita nimekirja</button>
                </div>
            </div>
            <div class="item-fluid">
                <canvas id="chartCanvas" style="height:40vh; width:80%"></canvas>
            </div>
        </div>
    `;
    document.querySelector('main').innerHTML = chartHtml;

    let ctx = document.querySelector('#chartCanvas').getContext('2d');
    let chartData = composeChartData(items);
    new Chart(ctx, {
        type: 'pie',
        data: chartData,
        options: {}
    });
}

/* 
    --------------------------------------------
    VALIDATION FUNCTIONS
    --------------------------------------------
*/

function validateItem(item) {
    let errors = [];

    if (item.name.length < 2) {
        errors.push('Ebakorrektne nimi (nõutud vähemalt kaks tähemärki)!');
    }

    if (item.established.length < 2) {
        errors.push('Ebakorrektne asutamisaasta!');
    }

    displayErrorBox(errors);
    return errors == 0;
}

/* 
    --------------------------------------------
    CHART FUNCTIONS
    --------------------------------------------
*/

function generateRandomColor() {
    let r = Math.floor(Math.random() * 255);
    let g = Math.floor(Math.random() * 255);
    let b = Math.floor(Math.random() * 255);
    return `rgb(${r},${g},${b})`;
}

function composeChartDataset(items) {
    if (items != null && items.length > 0) {
        let data = items.map(item => Math.round(item.marketCapitalization));
        let backgroundColors = items.map(generateRandomColor);
        return [{
            data: data,
            backgroundColor: backgroundColors,
        }];
    }
    return [];
}

function composeChartLabels(items) {
    return items != null && items.length > 0 ? items.map(item => item.name) : [];
}

function composeChartData(companies) {
    return {
        datasets: composeChartDataset(companies),
        labels: composeChartLabels(companies)
    };
}
